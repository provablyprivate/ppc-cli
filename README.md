# PPC-CLI

PPC-CLI is a command-line interface for using PPC. 

## Getting PPC-CLI

In order to run PPC-CLI you can either
pull the repository and compile it yourself, or simply download the binary executable from
the 'binary\_executable' directory (recommended). Note that the name of this binary will be
ppc\_cli, rather tham ppc-cli.

## Using PPC-CLI

PPC-CLI is a directory based command-line interface, i.e when used, it creates a .ppc directory
where the different necessary parts for usage are stored. For a list of commands and arguments,
you can use 'ppc\_cli -h', and 'ppc\_cli \<SUBCOMMAND\> -h' for further information regarding
specific subcommands.

Here is an example of the 'message retrieve' subcommand:
ppc\_cli message retrieve Agent2 Agent1 Certificate Item1

Which looks in the possession file and tries to retrieve the first instance of the
(simplified)  message:

Sender: _ Receiver: Agent2 Term: Certificate(Agent1, Item1).
