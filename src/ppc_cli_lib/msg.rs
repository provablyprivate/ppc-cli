use std::{
    env,
    fs::{
        read_to_string,
        File,
    },
    io::{
        self,
        Write,
    },
    path::{
        Path,
        PathBuf,
    },
    str::from_utf8,
};

use clap::ArgMatches;
use io::stdout;
use ppc::{
    message::Message,
    Architecture,
    Participant,
    PossessionHandler,
    Term,
    Type,
};

use crate::arch::{
    check_for_ppc,
    get_architecture_agents,
};
use crate::clikeys::CliKeys;
use crate::clipossessions::CliPossessions;
pub fn cmd_msg(matches: &ArgMatches) {
    match matches.subcommand() {
        Some(("create", matches)) => msg_create(matches),
        Some(("certify", matches)) => msg_certify(matches),
        Some(("decertify", matches)) => msg_decertify(matches),
        Some(("prove", matches)) => msg_prove(matches),
        Some(("validate", matches)) => msg_validate(matches),
        Some(("verify", matches)) => msg_verify(matches),
        Some(("receive", matches)) => msg_receive(matches),
        Some(("retrieve", matches)) => msg_retrieve(matches),
        _ => {
            println!(
                "Unrecognized command, try 'ppc --help message' for a list of \
                 valid commands"
            )
        }
    }
}

//Builds a ppc participant from the .ppc directory path.
fn build_ppc_participant(
    ppc_path: PathBuf,
) -> Result<Participant<CliKeys, CliPossessions>, String> {
    let mut arch_path = ppc_path.clone();
    let mut poss_path = ppc_path.clone();
    arch_path.push("architecture");
    poss_path.push("possessions");
    let keys = CliKeys::new(
        ppc_path,
        get_architecture_agents(&arch_path).map_err(|e| e.to_string())?,
    );
    let possessions = CliPossessions::new(&poss_path);
    let arch = serde_json::from_str::<Architecture>(
        &read_to_string(&arch_path).map_err(|e| e.to_string())?,
    )
    .map_err(|e| e.to_string())?;
    Ok(Participant::new(arch, keys, Some(possessions)))
}

fn build_messages_from_atomic(
    message: Message<Vec<u8>>,
    mut cli_participant: Participant<CliKeys, CliPossessions>,
    agents: Vec<String>,
    sender: &String,
) {
    for agent in &agents {
        match cli_participant
            .build_message()
            .sender(sender.clone().to_string())
            .receiver(agent.clone())
            .argument(message.clone())
            .unwrap()
            .certify(agent.clone())
            .sign()
        {
            Ok(_) => {}
            Err(_) => {}
        }
    }
    for agent in &agents {
        match cli_participant
            .build_message()
            .sender(sender.clone())
            .receiver(agent.clone())
            .argument(message.clone())
            .unwrap()
            .prove(sender.clone())
            .sign()
        {
            Ok(_) => {}
            Err(_) => {}
        }
    }
}

fn msg_create(matches: &ArgMatches) {
    let msg_term = matches.value_of("ITEM TYPE").unwrap().to_string();
    let msg_body = matches.value_of("BODY").unwrap().to_string();
    let sender = matches.value_of("SENDER").unwrap().to_string();
    let receiver = matches.value_of("RECEIVER").unwrap().to_string();

    let mut ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let mut cli_participant = match build_ppc_participant(ppc_path.clone()) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };

    let term = Term::atomic(msg_term, msg_body.as_bytes());
    let term = term.map(|body| body.into());

    let output = matches.value_of("OUTPUT");
    ppc_path.push("architecture");
    
    if sender != receiver {
        panic!("Can not create atomic types not directed towards yourself.");
    }

    match cli_participant
        .build_message()
        .sender(sender.clone())
        .receiver(receiver.clone())
        .term(term)
        .sign()
    {
        Ok(msg) => {
            match output {
                Some(out) => match File::create(out) {
                    Ok(mut file) => file
                        .write_all(
                            &serde_json::to_string(&msg).unwrap().as_bytes(),
                        )
                        .expect("Failed to write to file!"),
                    Err(error) => panic!("{}", error.to_string()),
                },
                None => stdout()
                    .write_all(&serde_json::to_string(&msg).unwrap().as_bytes())
                    .expect("Failed to write to standard out!"),
            }
            if matches.is_present("BUILD") {
                build_messages_from_atomic(
                    msg.clone(),
                    cli_participant,
                    get_architecture_agents(&ppc_path).unwrap(),
                    &sender,
                );
            }
        }
        Err(error) => panic!("{}", error.to_string()),
    };
}

fn msg_certify(matches: &ArgMatches) {
    let message = matches.value_of("MESSAGE").unwrap();
    let message = if matches.is_present("FILE") {
        serde_json::from_str::<Message<Vec<u8>>>(
            &read_to_string(message).unwrap(),
        )
        .expect("Invalid message")
    } else {
        serde_json::from_str::<Message<Vec<u8>>>(&message)
            .expect("Invalid message")
    };

    let sender = matches.value_of("SENDER").unwrap().to_string();
    let receiver = matches.value_of("RECEIVER").unwrap().to_string();

    let ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let mut cli_participant = match build_ppc_participant(ppc_path) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };

    let output = matches.value_of("OUTPUT");

    match cli_participant
        .build_message()
        .sender(sender)
        .receiver(receiver.clone())
        .argument(message.clone())
        .unwrap()
        .certify(receiver.clone())
        .sign()
    {
        Ok(msg) => match output {
            Some(out) => match File::create(out) {
                Ok(mut file) => file
                    .write_all(&serde_json::to_string(&msg).unwrap().as_bytes())
                    .expect("Failed to write to file!"),
                Err(error) => panic!("{}", error.to_string()),
            },
            None => stdout()
                .write_all(&serde_json::to_string(&msg).unwrap().as_bytes())
                .expect("Failed to write to standard out!"),
        },
        Err(error) => panic!("{}", error.to_string()),
    };
}

fn msg_decertify(matches: &ArgMatches) {
    let message = matches.value_of("MESSAGE").unwrap();
    let message = if matches.is_present("FILE") {
        serde_json::from_str::<Message<Vec<u8>>>(
            &read_to_string(message).unwrap(),
        )
        .expect("Invalid message")
    } else {
        serde_json::from_str::<Message<Vec<u8>>>(&message)
            .expect("Invalid message")
    };

    let sender = matches.value_of("SENDER").unwrap().to_string();
    let receiver = matches.value_of("RECEIVER").unwrap().to_string();

    if sender != receiver {
        panic!("Can not create atomic types not directed towards yourself.");
    }
    
    let mut ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let mut cli_participant = match build_ppc_participant(ppc_path.clone()) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };

    let output = matches.value_of("OUTPUT");
    ppc_path.push("architecture");
    match cli_participant
        .build_message()
        .sender(sender.clone())
        .receiver(receiver.clone())
        .argument(message.clone())
        .unwrap()
        .decertify()
        .sign()
    {
        Ok(msg) => {
            match output {
                Some(out) => match File::create(out) {
                    Ok(mut file) => file
                        .write_all(
                            &serde_json::to_string(&msg).unwrap().as_bytes(),
                        )
                        .expect("Failed to write to file!"),
                    Err(error) => panic!("{}", error.to_string()),
                },
                None => stdout()
                    .write_all(&serde_json::to_string(&msg).unwrap().as_bytes())
                    .expect("Failed to write to standard out!"),
            }
            if matches.is_present("BUILD") {
                build_messages_from_atomic(
                    msg.clone(),
                    cli_participant,
                    get_architecture_agents(&ppc_path).unwrap(),
                    &sender,
                );
            }
        }
        Err(error) => panic!("{}", error.to_string()),
    };
}

fn msg_prove(matches: &ArgMatches) {
    let message = matches.value_of("MESSAGE").unwrap();
    let message = if matches.is_present("FILE") {
        serde_json::from_str::<Message<Vec<u8>>>(
            &read_to_string(message).unwrap(),
        )
        .expect("Invalid message")
    } else {
        serde_json::from_str::<Message<Vec<u8>>>(&message)
            .expect("Invalid message")
    };

    let sender = matches.value_of("SENDER").unwrap().to_string();
    let receiver = matches.value_of("RECEIVER").unwrap().to_string();

    let ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let mut cli_participant = match build_ppc_participant(ppc_path) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };

    let output = matches.value_of("OUTPUT");

    match cli_participant
        .build_message()
        .sender(sender.clone())
        .receiver(receiver.clone())
        .argument(message.clone())
        .unwrap()
        .prove(sender.clone())
        .sign()
    {
        Ok(msg) => match output {
            Some(out) => match File::create(out) {
                Ok(mut file) => file
                    .write_all(&serde_json::to_string(&msg).unwrap().as_bytes())
                    .expect("Failed to write to file!"),
                Err(error) => panic!("{}", error.to_string()),
            },
            None => stdout()
                .write_all(&serde_json::to_string(&msg).unwrap().as_bytes())
                .expect("Failed to write to standard out!"),
        },
        Err(error) => panic!("{}", error.to_string()),
    };
}

fn msg_validate(matches: &ArgMatches) {
    let message = matches.value_of("MESSAGE").unwrap();
    let message = if matches.is_present("FILE") {
        serde_json::from_str::<Message<Vec<u8>>>(
            &read_to_string(message).unwrap(),
        )
        .expect("Invalid message")
    } else {
        serde_json::from_str::<Message<Vec<u8>>>(&message)
            .expect("Invalid message")
    };

    let ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let cli_participant = match build_ppc_participant(ppc_path) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };
    match cli_participant.validate(&message) {
        Ok(_) => println!("Message is valid."),
        Err(error) => println!("Message invalid: {}", error),
    }
}

fn msg_verify(matches: &ArgMatches) {
    let message = matches.value_of("MESSAGE").unwrap();
    let message = if matches.is_present("FILE") {
        serde_json::from_str::<Message<Vec<u8>>>(
            &read_to_string(message).unwrap(),
        )
        .expect("Invalid message")
    } else {
        serde_json::from_str::<Message<Vec<u8>>>(&message)
            .expect("Invalid message")
    };

    let ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let cli_participant = match build_ppc_participant(ppc_path) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };
    match cli_participant.verify(message) {
        Ok(_) => println!("Proof is valid."),
        Err(error) => println!("Proof invalid: {}", error),
    }
}

fn msg_receive(matches: &ArgMatches) {
    let message = matches.value_of("MESSAGE").unwrap();
    let message = if matches.is_present("FILE") {
        serde_json::from_str::<Message<Vec<u8>>>(
            &read_to_string(message).unwrap(),
        )
        .expect("Invalid message")
    } else {
        serde_json::from_str::<Message<Vec<u8>>>(&message)
            .expect("Invalid message")
    };
    let ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let mut cli_participant = match build_ppc_participant(ppc_path) {
        Ok(part) => part,
        Err(error) => panic!("{}", error),
    };
    match cli_participant.accept(message) {
        Ok(_) => println!("Message added into possessions."),
        Err(error) => panic!(
            "Message could not be added to possessions: {}",
            error.to_string()
        ),
    }
}

fn msg_retrieve(matches: &ArgMatches) {
    let _type = matches.value_of("MESSAGE TYPE").unwrap();
    let item = matches.value_of("ITEM TYPE").unwrap();
    let m_id = matches.value_of("MESSAGE ID").unwrap();
    let i_id = matches.value_of("ITEM ID").unwrap();
    let ppc_path = match matches.value_of("PPC") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");

    let item = match _type {
        "Atomic" => Type::atomic(item),
        "Certificate" => Type::certificate(i_id, item),
        "Proof" => Type::proof(i_id, item),
        _ => panic!("Could not parse given type."),
    };

    let mut arch_path = ppc_path.clone();
    let mut poss_path = ppc_path.clone();
    arch_path.push("architecture");
    poss_path.push("possessions");
    let keys =
        CliKeys::new(ppc_path, get_architecture_agents(&arch_path).unwrap());
    let possessions = CliPossessions::new(&poss_path);
    let possession =
        match possessions.query_possession(&keys, &item, &m_id.to_string()) {
            Ok(success) => match success {
                Some(poss) => poss,
                None => panic!("Could not find possession."),
            },
            Err(error) => panic!("{}", error.to_string()),
        };

    let writable_possession = if matches.is_present("TEXT") {
        serde_json::to_string(
            &possession.map(|body| from_utf8(&body).unwrap().to_string()),
        )
        .unwrap()
    } else {
        serde_json::to_string(&possession).unwrap()
    };

    let output = matches.value_of("OUTPUT");

    match output {
        Some(out) => match File::create(out) {
            Ok(mut file) => file
                .write_all(&writable_possession.as_bytes())
                .expect("Failed to write to file!"),
            Err(error) => panic!("{}", error.to_string()),
        },
        None => stdout()
            .write_all(&writable_possession.as_bytes())
            .expect("Failed to write to standard out!"),
    }
}
