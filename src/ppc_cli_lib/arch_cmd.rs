use std::env;
use std::fs::{
    copy,
    create_dir,
    remove_dir_all,
    File,
};
use std::io::Write;
use std::path::{
    Path,
    PathBuf,
};

use clap::ArgMatches;
use ppc::Identity;

use crate::arch::*;

///Base ppc command, used for commands which deal with the .ppc directory.
pub fn cmd_arch(matches: &ArgMatches) {
    match matches.subcommand() {
        Some(("init", matches)) => cmd_init(matches),
        Some(("hash", matches)) => cmd_hash(matches),
        _ => {
            println!(
                "Unrecognized command, try 'ppc --help message' for a list of \
                 valid commands"
            )
        }
    }
}

///Command used to initiate the .ppc directory. Takes as input:
///A file containing an architecture.
///
///A directory containing two sub-directories which contain all public keys to
/// be used (these two sub-directories are to be named 'sing_keys' and
/// 'encrypt_keys', and both are to contain one public key for each agent with
/// the file extension '.pub'. A directory containing the private keys of the
/// agent which the caller of the command is to represent in the architecture
/// (again, split into directories 'sign_keys', and 'encrypt_keys', but only
/// containing the private keys to be used, with the name of the agent and no
/// file extensions.
///
///And finally, the name of the agent whom the user is to represent in the
/// architecture, as they appear in the architecture file.
///
///The .ppc directory created will then contain:
///
///An architecture file containing a JSON serialized architecture.
///
///A 'keys/' directory containing all keys to be used. Note that the private
/// keys used are also present in this directory, therefore it should not be
/// shared with anyone.
///
///A 'possessions' file, which keeps track of the possessions of the user.
pub fn cmd_init(matches: &ArgMatches) {
    let mut current_dir = env::current_dir().unwrap();
    match check_for_ppc(&current_dir) {
        Some(mut ppc_path) => {
            ppc_path.pop();
            if &ppc_path == &current_dir {
                panic!(".ppc already found in this directory");
            } else {
                println!(
                    ".ppc found in some ancestor directory: {:?}, continuing \
                     anyway...",
                    &ppc_path
                );
            }
        }
        None => {
            println!("No ppc found in tree");
        }
    }
    let architecture_file =
        matches.value_of("ARCHITECTURE FILE").unwrap().to_string();
    let key_directory = matches.value_of("KEY DIRECTORY").unwrap().to_string();
    let my_key_directory =
        matches.value_of("MY PRIVATE KEYS").unwrap().to_string();
    let my_id = matches.value_of("MY ID").unwrap().to_string();
    match build_ppc(
        current_dir.clone(),
        architecture_file,
        key_directory,
        my_key_directory,
        my_id,
    ) {
        Ok(result) => {
            println!("{}", result);
        }
        Err(error) => {
            println!(".ppc build failed: {:?}", error);
            current_dir.push(".ppc");
            match remove_dir_all(current_dir) {
                Ok(_) => println!(".ppc directory deleted"),
                Err(error) => panic!(
                    "Could not remove partially completed .ppc file: {:?}",
                    error
                ),
            }
        }
    }
}

///Command for getting the hash of a .ppc directory (only the static
///components are used, such as public keys and architecture file.
fn cmd_hash(matches: &ArgMatches) {
    let arch_path = match matches.value_of("ARCHITECTURE") {
        Some(path) => check_for_ppc(Path::new(path)),
        None => check_for_ppc(&env::current_dir().unwrap()),
    }
    .expect("Could not find achitecture!");
    println!("{}", hash_ppc(arch_path).unwrap());
}

//Function responsible for constructing the .ppc directory, returns Ok on
//successful construction, on failed construction the partially constructed
//.ppc directory should be removed by the calling function.
fn build_ppc(
    mut dir: PathBuf,
    architecture_str: String,
    key_str: String,
    priv_key_str: String,
    my_id: String,
) -> Result<String, String> {
    let key_path = Path::new(&key_str);
    let architecture_path = Path::new(&architecture_str);
    let result = verify_keys_against_architecture(
        key_path.clone().to_path_buf(),
        architecture_path.clone(),
    )?;
    println!("{}", result);
    dir.push(".ppc");
    match create_dir(&dir) {
        Ok(_) => {
            println!(".ppc directory created at {:?}", &dir);
        }
        Err(error) => {
            println!(".ppc directory creation failed");
            return Err(error.to_string());
        }
    }
    dir.push("architecture");
    match File::create(&dir) {
        Ok(_) => match copy(architecture_path, &dir) {
            Ok(_) => {
                println!("Blueprint file created");
            }
            Err(error) => {
                println!("Blueprint file creation failed on copy");
                return Err(error.to_string());
            }
        },
        Err(error) => {
            println!("Blueprint file creation failed on build");
            return Err(error.to_string());
        }
    }
    dir.pop();
    dir.push("possessions");
    match File::create(&dir) {
        Ok(mut file) => match file.write("{}".as_bytes()) {
            Ok(_) => println!("Possessions file created"),
            Err(error) => return Err(error.to_string()),
        },
        Err(error) => {
            println!("Possessions file creation failed");
            return Err(error.to_string());
        }
    }
    dir.pop();
    dir.push("keys");
    let mut key_path_buf = key_path.to_path_buf();
    match create_dir(&dir) {
        Ok(_) => {
            let agents = get_architecture_agents(&architecture_path)?;
            dir.push("sign_keys");
            create_dir(&dir).map_err(|e| e.to_string())?;
            key_path_buf.push("sign_keys");
            for mut agent in agents.clone() {
                agent.push_str(".pub");
                key_path_buf.push(&agent);
                println!("Copying: {:?}...", &key_path_buf);
                dir.push(&agent);
                copy(&key_path_buf, &dir).map_err(|e| e.to_string())?;
                println!("Done");
                key_path_buf.pop();
                dir.pop();
            }
            dir.pop();
            dir.push("encrypt_keys");
            create_dir(&dir).map_err(|e| e.to_string())?;
            key_path_buf.pop();
            key_path_buf.push("encrypt_keys");
            for mut agent in agents.clone() {
                agent.push_str(".pub");
                key_path_buf.push(&agent);
                println!("Copying: {:?}...", &key_path_buf);
                dir.push(&agent);
                copy(&key_path_buf, &dir).map_err(|e| e.to_string())?;
                println!("Done");
                key_path_buf.pop();
                dir.pop();
            }
            key_path_buf.pop();
            dir.pop();
            dir.pop();
            println!("Keys directory done");
        }
        Err(error) => {
            println!("Key directory creation failed");
            return Err(error.to_string());
        }
    }
    let priv_key_path = Path::new(&priv_key_str);
    let result =
        add_private_keys(dir.clone(), priv_key_path.to_path_buf(), &my_id)?;
    println!("{}", result);
    let result = hash_ppc(dir)?;
    println!("PPC Hash: {}", result);
    Ok(".ppc directory successfully created.".to_string())
}

//Adds the private keys to the .ppc/keys/* directories based on their naming
// scheme.
fn add_private_keys(
    mut ppc_path: PathBuf,
    mut my_key_path: PathBuf,
    id: &Identity,
) -> Result<String, String> {
    my_key_path.push("sign");
    ppc_path.push("keys");
    ppc_path.push("sign_keys");
    ppc_path.push(id);
    println!("Copying: {:?} to {:?}...", &my_key_path, &ppc_path);
    copy(&my_key_path, &ppc_path).map_err(|e| e.to_string())?;
    println!("Done");
    ppc_path.pop();
    ppc_path.pop();
    ppc_path.push("encrypt_keys");
    ppc_path.push(id);
    my_key_path.pop();
    my_key_path.push("encrypt");
    println!("Copying: {:?} to {:?}...", &my_key_path, &ppc_path);
    copy(&my_key_path, &ppc_path).map_err(|e| e.to_string())?;
    println!("Done");
    Ok("Private keys imported.".to_string())
}
