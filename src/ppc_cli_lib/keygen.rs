use std::fs;
use std::io::Write;

use base64::encode;
use clap::ArgMatches;
use ppc::KeyQuartette;
/// Generate a ppc keypair
use rand;
pub fn cmd_keygen(matches: &ArgMatches) {
    let identity = matches.value_of("IDENTITY").unwrap();
    let key_path = matches.value_of("KEY PATH").unwrap();

    let mut rng = rand::thread_rng();
    let kq = KeyQuartette::gen(&mut rng);
    // Add to encrypt_keys and sign_keys IDENTITY.pub
    // Add folder IDENTITY with sign and encrypt private keys

    // Create key directories
    fs::create_dir_all(key_path).expect("Unable to create key directory");
    fs::create_dir_all(format!("{}/encrypt_keys", key_path))
        .expect("Unable to create encrypt_keys subdirectory");
    fs::create_dir_all(format!("{}/sign_keys", key_path))
        .expect("Unable to create sign_keys subdirectory");
    fs::create_dir_all(format!("{}/{}", key_path, identity))
        .expect("Unable to create IDENTITY subdirectory");

    // Write public encryption key
    let mut pub_encrypt_file =
        fs::File::create(format!("{}/encrypt_keys/{}.pub", key_path, identity))
            .expect("Unable to create file");
    pub_encrypt_file
        .write(encode(&kq.public_encryption_data).as_bytes())
        .expect("Unable to write to file");

    // Write private encryption key
    let mut priv_encrypt_file =
        fs::File::create(format!("{}/{}/encrypt", key_path, identity))
            .expect("Unable to create file");
    priv_encrypt_file
        .write(encode(&kq.secret_encryption_data).as_bytes())
        .expect("Unable to write to file");

    // Write public sign key
    let mut pub_sign_file =
        fs::File::create(format!("{}/sign_keys/{}.pub", key_path, identity))
            .expect("Unable to create file");
    pub_sign_file
        .write(encode(&kq.public_sign_data).as_bytes())
        .expect("Unable to write to file");

    // Write private sign key
    let mut priv_sign_file =
        fs::File::create(format!("{}/{}/sign", key_path, identity))
            .expect("Unable to create file");
    priv_sign_file
        .write(encode(&kq.secret_sign_data).as_bytes())
        .expect("Unable to write to file");
}
