use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::{
    read_to_string,
    File,
};
use std::io::prelude::*;
use std::path::PathBuf;

use ppc::{
    message::Message,
    message::Type,
    Identity,
    Keyring,
    PossessionHandler,
};

use crate::arch::build_msg_name_term;

///For handling CliPossession errors.
#[derive(Debug, Clone)]
pub enum CliPossessionError {
    FileError(String),
    PossessionError(String),
    FormattingError(String),
}

impl fmt::Display for CliPossessionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use CliPossessionError::*;

        match self {
            FileError(path) => write!(f, "File not found on path: {}", path),
            PossessionError(possession) => {
                write!(f, "Possession error: {:?}", possession)
            }
            FormattingError(input) => {
                write!(f, "Error when trying to format file input: {:?}", input)
            }
        }
    }
}

impl Error for CliPossessionError {}

///For dealing with possessions, uses the PossessionHandler trait to
///verify message construction and addition/querying of messages to
///possessions.
#[derive(Debug, Clone)]
pub struct CliPossessions {
    possessions_filepath: PathBuf,
}

///Constructs a new CliPossession with a given possessions file.
impl CliPossessions {
    pub fn new(path: &PathBuf) -> CliPossessions {
        CliPossessions {
            possessions_filepath: path.clone(),
        }
    }
}

impl PossessionHandler for CliPossessions {
    type Err = CliPossessionError;
    type Body = Vec<u8>;
    //Used to query possessions from the possession file.
    //NOTE currently all possessions with the same type are gathered under the
    // same vector, this leads to only the first message encountered with
    // the input type getting returned, This might need to be fixed.
    fn query_possession(
        &self,
        keyring: &impl Keyring,
        _type: &Type,
        receiver: &Identity,
    ) -> Result<Option<Message<Self::Body>>, Self::Err> {
        let possessions = match read_to_string(&self.possessions_filepath) {
            Ok(contents) => {
                match serde_json::from_str::<
                    HashMap<String, Vec<Message<Self::Body>>>,
                >(&contents)
                {
                    Ok(hashmap_contents) => hashmap_contents,
                    Err(_) => {
                        return Err(CliPossessionError::FormattingError(
                            contents,
                        ))
                    }
                }
            }
            Err(_) => {
                return Err(CliPossessionError::FileError(
                    self.possessions_filepath.to_str().unwrap().to_string(),
                ))
            }
        };
        let message_id = build_msg_name_term(_type);
        let mut found_messages = Vec::new();
        match possessions.get(&message_id) {
            Some(messages) => {
                for message in messages {
                    match message.receiver(keyring) {
                        Ok(rec) => {
                            if rec == receiver {
                                found_messages.push(message);
                            }
                        }
                        Err(error) => {
                            return Err(CliPossessionError::PossessionError(
                                error.to_string(),
                            ))
                        }
                    }
                }
            }
            None => return Ok(None),
        }
        if found_messages.len() == 0 {
            Ok(None)
        } else if found_messages.len() == 1 {
            Ok(Some(found_messages[0].clone()))
        } else {
            Ok(Some(found_messages[0].clone()))
            //Err(CliPossessionError::PossessionError("Multiple messages of
            // this type \    and receiver exist, \
            //    please manually retrieve the desired message from the \
            //    possessions file instead.".to_string()))
        }
    }
    //Reads possession file into a hashmap, adds the possession, then rewrites
    // the file. Currently all possessions with the same type are put into
    // a vector.
    fn insert_possession<CliKeys: Keyring>(
        &mut self,
        keyring: &CliKeys,
        message: Message<Self::Body>,
    ) -> Result<(), Self::Err> {
        let mut possessions = match read_to_string(&self.possessions_filepath) {
            Ok(contents) => match serde_json::from_str::<
                HashMap<String, Vec<Message<Self::Body>>>,
            >(&contents)
            {
                Ok(hashmap_contents) => hashmap_contents,
                Err(_) => {
                    return Err(CliPossessionError::FormattingError(contents))
                }
            },
            Err(_) => {
                return Err(CliPossessionError::FileError(
                    self.possessions_filepath.to_str().unwrap().to_string(),
                ))
            }
        };
        let message_id = match message.term(keyring) {
            Ok(term) => build_msg_name_term(term._type()),
            Err(_) => {
                return Err(CliPossessionError::PossessionError(
                    serde_json::to_string(&message).unwrap(),
                ))
            }
        };
        match possessions.get_mut(&message_id) {
            Some(messages) => {
                messages.push(message);
            }
            None => {
                possessions.insert(message_id, vec![message]);
            }
        }
        match serde_json::to_string(&possessions) {
            Ok(possession_string) => {
                match File::create(&self.possessions_filepath) {
                    Ok(mut poss_file) => match poss_file
                        .write_all(possession_string.as_bytes())
                    {
                        Ok(_) => Ok(()),
                        Err(error) => Err(CliPossessionError::FileError(
                            error.to_string(),
                        )),
                    },
                    Err(_) => Err(CliPossessionError::FileError(
                        self.possessions_filepath.to_str().unwrap().to_string(),
                    )),
                }
            }
            Err(_) => Err(CliPossessionError::FormattingError(format!(
                "{:?}",
                &possessions
            ))),
        }
    }
}
