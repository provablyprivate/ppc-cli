use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::read;
use std::path::PathBuf;

use base64;
use ppc::{
    Identity,
    Keyring,
};

use crate::arch::filter_line_endings;

///Used for keyring errors.
#[derive(Debug, Clone)]
pub enum KeyringError {
    MissingIdentity(String),
    MissingKey(String),
}

impl fmt::Display for KeyringError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use KeyringError::*;

        match self {
            MissingIdentity(error) => {
                write!(f, "Identity missing in keyring: {}", error)
            }
            MissingKey(error) => write!(f, "Key missing in keyring: {}", error),
        }
    }
}

impl Error for KeyringError {}

//Used by CliKeys to get keys for an agent.
#[derive(Debug, Clone)]
struct Keys {
    pub private_sign_data: Result<Vec<u8>, KeyringError>,
    pub public_sign_data: Result<Vec<u8>, KeyringError>,
    pub private_encryption_data: Result<Vec<u8>, KeyringError>,
    pub public_encryption_data: Result<Vec<u8>, KeyringError>,
}

//Gets the keys for an agent if they exist in the keys directory.
impl Keys {
    pub fn get(mut path: PathBuf, id: &Identity) -> Keys {
        let private_sign = id.clone();
        let mut public_sign = id.clone();
        let private_encrypt = id.clone();
        let mut public_encrypt = id.clone();
        public_sign.push_str(".pub");
        public_encrypt.push_str(".pub");
        path.push("sign_keys");
        path.push(private_sign);
        let private_sign_data = match read(&path) {
            Ok(psd) => Ok(base64::decode(filter_line_endings(psd)).unwrap()),
            Err(_) => Err(KeyringError::MissingKey(format!(
                "private signing key for {} missing.",
                id
            ))),
        };
        path.pop();
        path.push(public_sign);
        let public_sign_data = match read(&path) {
            Ok(psd) => Ok(base64::decode(filter_line_endings(psd)).unwrap()),
            Err(_) => Err(KeyringError::MissingKey(format!(
                "public signing key for {} missing.",
                id
            ))),
        };
        path.pop();
        path.pop();
        path.push("encrypt_keys");
        path.push(private_encrypt);
        let private_encryption_data = match read(&path) {
            Ok(psd) => Ok(base64::decode(filter_line_endings(psd)).unwrap()),
            Err(_) => Err(KeyringError::MissingKey(format!(
                "private encryption key for {} missing.",
                id
            ))),
        };
        path.pop();
        path.push(public_encrypt);
        let public_encryption_data = match read(&path) {
            Ok(psd) => Ok(base64::decode(filter_line_endings(psd)).unwrap()),
            Err(_) => Err(KeyringError::MissingKey(format!(
                "public encryption key for {} missing.",
                id
            ))),
        };
        Keys {
            private_sign_data,
            public_sign_data,
            private_encryption_data,
            public_encryption_data,
        }
    }
}

///Keyring implementation for PPC-CLI. This struct will keep all keys found in
///the _/keys directory and can be queried for the encryption or signing
///keys, both public and private. KeyringErrors will be returned if the
///keys are not available, or if the agent is non-existant.
#[derive(Debug, Clone)]
pub struct CliKeys {
    keys: HashMap<Identity, Keys>,
}

///the new function returns a CliKeys structure defined by the directory
///where the 'keys/' directory is to be found, as well as the identities
///of all agents in the architecture.
impl CliKeys {
    pub fn new(mut path: PathBuf, ids: Vec<Identity>) -> CliKeys {
        path.push("keys");
        let mut r_keys = HashMap::new();
        for id in ids {
            r_keys.insert(id.clone(), Keys::get(path.clone(), &id));
        }
        CliKeys { keys: r_keys }
    }
}

///See Keyring in ppc-core for more information.
impl Keyring for CliKeys {
    type Err = KeyringError;
    fn secret_sign_bytes(&self, id: &Identity) -> Result<&[u8], Self::Err> {
        match &self.keys.get(id).unwrap().private_sign_data {
            Ok(result) => Ok(result.as_slice()),
            Err(error) => Err(KeyringError::MissingIdentity(error.to_string())),
        }
    }
    fn public_sign_bytes(&self, id: &Identity) -> Result<&[u8], Self::Err> {
        match &self.keys.get(id).unwrap().public_sign_data {
            Ok(result) => Ok(result.as_slice()),
            Err(error) => Err(KeyringError::MissingIdentity(error.to_string())),
        }
    }
    fn secret_encryption_bytes(
        &self,
        id: &Identity,
    ) -> Result<&[u8], Self::Err> {
        match &self.keys.get(id).unwrap().private_encryption_data {
            Ok(result) => Ok(result.as_slice()),
            Err(error) => Err(KeyringError::MissingIdentity(error.to_string())),
        }
    }
    fn public_encryption_bytes(
        &self,
        id: &Identity,
    ) -> Result<&[u8], Self::Err> {
        match &self.keys.get(id).unwrap().public_encryption_data {
            Ok(result) => Ok(result.as_slice()),
            Err(error) => Err(KeyringError::MissingIdentity(error.to_string())),
        }
    }
}
