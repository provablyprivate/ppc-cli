use std::collections::{
    hash_map::DefaultHasher,
    HashMap,
};
use std::fs::{
    read,
    read_to_string,
};
use std::hash::{
    Hash,
    Hasher,
};
use std::path::{
    Path,
    PathBuf,
};

use base64;
use ppc::{
    message::Message,
    message::Type,
    Architecture,
    Identity,
};
use serde::Serialize;

///Filters illegal line endings as defined.
pub fn filter_line_endings(bs: Vec<u8>) -> Vec<u8> {
    let illegal_characters = ['\n' as u8, '\r' as u8];
    let test = bs
        .into_iter()
        .filter(|b| !illegal_characters.contains(b))
        .collect::<Vec<u8>>();
    test
}

///Used to hash a .ppc file (using only the static components) into a hash
/// which can be used as to verify that two agents are using the same
/// validator.
pub fn hash_ppc(mut ppc_path: PathBuf) -> Result<String, String> {
    ppc_path.push("architecture");
    let mut pre_hash_string = read_to_string(&ppc_path).unwrap();
    let agents = get_architecture_agents(&ppc_path)?;
    ppc_path.pop();
    ppc_path.push("keys");
    ppc_path.push("sign_keys");
    for mut agent in agents.clone() {
        agent.push_str(".pub");
        ppc_path.push(agent);
        if !ppc_path.exists() {
            return Err(format!("Could not find key file {:?}", &ppc_path));
        }
        pre_hash_string.push_str(&read_to_string(&ppc_path).unwrap());
        ppc_path.pop();
    }
    ppc_path.pop();
    ppc_path.push("encrypt_keys");
    for mut agent in agents {
        agent.push_str(".pub");
        ppc_path.push(agent);
        if !ppc_path.exists() {
            return Err(format!("Could not find key file {:?}", &ppc_path));
        }
        pre_hash_string.push_str(&read_to_string(&ppc_path).unwrap());
        ppc_path.pop();
    }
    let mut hasher = DefaultHasher::new();
    pre_hash_string.hash(&mut hasher);
    Ok(hasher.finish().to_string())
}

///Used to verify keys found in a key directories against an architecture. The
/// architecture is parsed for all agents and the key directory is checked to
/// make sure all agents have a .pub file of correct size. Checks encrypt/sign
/// key directories.
pub fn verify_keys_against_architecture(
    mut key_dir: PathBuf,
    architecture_file: &Path,
) -> Result<String, String> {
    let agents = get_architecture_agents(architecture_file)?;
    let mut key_type: Vec<&str> = Vec::new();
    key_type.push("sign_keys");
    key_type.push("encrypt_keys");
    for key_type_dir in &key_type {
        key_dir.push(key_type_dir);
        for mut agent in agents.clone() {
            agent.push_str(".pub");
            key_dir.push(agent);
            if !key_dir.exists() {
                return Err(format!("Could not find key file {:?}", &key_dir));
            }
            if base64::decode(filter_line_endings(read(&key_dir).unwrap()))
                .unwrap()
                .len()
                != 32
            {
                return Err(format!(
                    "Size of {:?} does not match required keysize",
                    &key_dir
                ));
            }
            key_dir.pop();
        }
        key_dir.pop();
    }
    Ok("All agent keys found.".to_string())
}

///Returns all constructors available to an agent and its interfaces.
pub fn get_architecture_agent_constructors(
    architecture_file: &Path,
    agent_id: &Identity,
) -> Result<
    (
        HashMap<Type, Vec<Type>>,
        HashMap<Type, Vec<Type>>,
        HashMap<Type, Vec<Type>>,
    ),
    String,
> {
    let architecture_string =
        read_to_string(architecture_file).map_err(|e| e.to_string())?;
    let arch = serde_json::from_str::<Architecture>(&architecture_string)
        .map_err(|e| e.to_string())?;
    let base_constructors = arch
        .get_agents()
        .get(agent_id)
        .ok_or("Invalid_agent_id")?
        .get_constructors();
    let input_constructors = arch
        .get_input_interfaces()
        .get(agent_id)
        .ok_or("Invalid_agent_id")?
        .get_constructors();
    let output_constructors = arch
        .get_output_interfaces()
        .get(agent_id)
        .ok_or("Invalid_agent_id")?
        .get_constructors();
    Ok((input_constructors, base_constructors, output_constructors))
}

///Gets all items needed to build a specific item. Based on constructors in
///the architecture file.
pub fn get_build_items_needed(
    term: &Type,
    arch_path: &PathBuf,
    creator: &Identity,
) -> Result<(Vec<Type>, Vec<Type>, Vec<Type>), String> {
    let (i_consts, b_consts, o_consts) =
        get_architecture_agent_constructors(arch_path, creator)?;
    let input_buildpaths: Vec<Type> =
        i_consts.get(term).cloned().unwrap_or(Vec::new());
    let base_buildpaths: Vec<Type> =
        b_consts.get(term).cloned().unwrap_or(Vec::new());
    let output_buildpaths: Vec<Type> =
        o_consts.get(term).cloned().unwrap_or(Vec::new());
    Ok((input_buildpaths, base_buildpaths, output_buildpaths))
}

///Used to get all agents in a architecture. This can be used for validation of
/// messages and such. This function returns a vector of strings on successful
/// parsing, each string will be the name of an agent, and each agent in the
/// architecture will be found in the vector.
pub fn get_architecture_agents(
    architecture_file: &Path,
) -> Result<Vec<String>, String> {
    let architecture_string =
        read_to_string(architecture_file).map_err(|e| e.to_string())?;
    let arch = serde_json::from_str::<Architecture>(&architecture_string)
        .map_err(|e| e.to_string())?;
    let agent_names = arch.get_agents().keys().cloned().collect();
    Ok(agent_names)
}

///Checks the current directory, and all the ancestors for the closest .ppc
///directory.
pub fn check_for_ppc(path: &Path) -> Option<PathBuf> {
    let mut r_val: Option<PathBuf> = None;
    if path.is_dir() {
        for ancestor in path.ancestors() {
            let ancestor_ppc = ancestor.join(".ppc");
            if ancestor_ppc.exists() {
                r_val = Some(ancestor_ppc);
                break;
            }
        }
    } else {
        r_val = None;
    }
    r_val
}

///Builds a message name from a term, used in identification
///of possessions as the key for the possession hashmap.
pub fn build_msg_name_term(term: &Type) -> String {
    match term {
        Type::Atomic(id) => {
            let mut return_string = "".to_string();
            return_string.push_str("Atomic");
            return_string.push_str(id);
            return_string
        }
        Type::Certificate(agent, inner) => {
            let mut return_string = "".to_string();
            return_string.push_str("Certificate");
            return_string.push_str(agent);
            return_string.push_str(&build_msg_name_term(inner));
            return_string
        }
        Type::Proof(agent, inner) => {
            let mut return_string = "".to_string();
            return_string.push_str("Proof");
            return_string.push_str(agent);
            return_string.push_str(&build_msg_name_term(inner));
            return_string
        }
    }
}

///Hashes a message into a string for id usage.
pub fn hash_msg<T: Serialize>(msg: &Message<T>) -> Result<String, String> {
    let pre_hash_string =
        serde_json::to_string(&msg).map_err(|e| e.to_string())?;
    let mut hasher = DefaultHasher::new();
    pre_hash_string.hash(&mut hasher);
    Ok(hasher.finish().to_string())
}
