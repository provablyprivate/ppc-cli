use std::{
    fs::{
        self,
        File,
    },
    io::Write,
};

use clap::ArgMatches;
use ppc::{
    abstract_syntax::{
        eval::eval_as,
        typechecker::typecheck_as,
    },
    expand::expand_architecture,
};
use ppc_dsl::{
    self,
    ppc_dsl::get_as,
};

pub fn cmd_dsl(matches: &ArgMatches) {
    let file_path = matches.value_of("DSL FILE").unwrap();
    let result_path = matches.value_of("RESULT FILE").unwrap();

    let dsl_contents = fs::read_to_string(file_path)
        .expect("Unable to read DSL file, maybe path is invalid?");

    let asppc = get_as(dsl_contents);
    typecheck_as(&asppc);
    let (architecture, negative_constraints, positive_constraints) =
        eval_as(asppc);
    let expanded_architecture = expand_architecture(
        architecture,
        negative_constraints,
        positive_constraints,
    );

    let mut result_file = File::create(result_path)
        .expect("Unable to create file at result path");
    result_file
        .write(
            serde_json::to_string(&expanded_architecture)
                .unwrap()
                .as_bytes(),
        )
        .expect("Failed to write to result file");
}
