extern crate clap;
use clap::{
    App,
    Arg,
};
use ppc_cli_lib::keygen::cmd_keygen;
use ppc_cli_lib::msg::cmd_msg;
use ppc_cli_lib::{
    arch_cmd::cmd_arch,
    dsl::cmd_dsl,
};

fn main() {
    let matches = App::new("ppc")
        .about("PPC CLI tool")
        .author("PPC Group")
        .subcommand(
            App::new("dsl")
                .about("Create architecture from DSL file")
                .args(&[
                    Arg::new("DSL FILE")
                        .about("Path to DSL file")
                        .required(true)
                        .index(1),
                    Arg::new("RESULT FILE")
                        .about(
                            "Path where result architecture should be stored",
                        )
                        .required(true)
                        .index(2),
                ]),
        )
        .subcommand(
            App::new("keygen")
                .about("Generate keypair usable by ppc")
                .args(&[
                    Arg::new("IDENTITY")
                        .about("Identity to make keys for")
                        .required(true)
                        .index(1),
                    Arg::new("KEY PATH")
                        .about("Path to place key files")
                        .required(true)
                        .index(2),
                ]),
        )
        .subcommand(
            App::new("validator")
                .about("Tool for dealing with PPC architectures")
                .subcommand(
                    App::new("init")
                        .about(
                            "Create arch file if one does not already exist.",
                        )
                        .args(&[
                            Arg::new("ARCHITECTURE FILE")
                                .about("Path to architecture file")
                                .required(true)
                                .index(1),
                            Arg::new("KEY DIRECTORY")
                                .about("Path to directory containing keys")
                                .required(true)
                                .index(2),
                            Arg::new("MY PRIVATE KEYS")
                                .about(
                                    "Path to directory containing your two \
                                     private keys",
                                )
                                .required(true)
                                .index(3),
                            Arg::new("MY ID")
                                .about(
                                    "The name with which you are identified \
                                     in the architecture",
                                )
                                .required(true)
                                .index(4),
                        ]),
                )
                .subcommand(
                    App::new("hash").about("Returns hashed architecture").arg(
                        Arg::new("PPC")
                            .about("Optional path to specify which .ppc to use")
                            .short('p')
                            .long("ppc")
                            .required(false),
                    ),
                ),
        )
        .subcommand(
            App::new("message")
                .about("Tool for dealing with PPC messages")
                .subcommand(
                    App::new("create").about("Create a new message").args(&[
                        Arg::new("SENDER")
                            .about("Identity to send from")
                            .required(true)
                            .index(1),
                        Arg::new("RECEIVER")
                            .about("Identity to send to")
                            .required(true)
                            .index(2),
                        Arg::new("ITEM TYPE")
                            .about("Message content")
                            .required(true)
                            .index(3),
                        Arg::new("BODY")
                            .about("Message content")
                            .required(true)
                            .index(4),
                        Arg::new("PPC")
                            .about("Optional path to specify which .ppc to use")
                            .short('p')
                            .long("ppc")
                            .required(false),
                        Arg::new("OUTPUT")
                            .about(
                                "Flag if output is to be directed someplace \
                                 other than standard output",
                            )
                            .short('o')
                            .long("output")
                            .takes_value(true)
                            .required(false),
                        Arg::new("BUILD")
                            .about(
                                "Flag to build all messages, both \
                                 certificates and proofs, possible using this \
                                 message.",
                            )
                            .short('b')
                            .long("build")
                            .takes_value(false)
                            .required(false),
                    ]),
                )
                .subcommand(
                    App::new("certify").about("Certify existing message").args(
                        &[
                            Arg::new("MESSAGE")
                                .about("Message to certify")
                                .required(true)
                                .index(1),
                            Arg::new("SENDER")
                                .about("Identity to send from")
                                .required(true)
                                .index(2),
                            Arg::new("RECEIVER")
                                .about("Identity to send to")
                                .required(true)
                                .index(3),
                            Arg::new("PPC")
                                .about(
                                    "Optional path to specify which .ppc to \
                                     use",
                                )
                                .short('p')
                                .long("ppc")
                                .required(false),
                            Arg::new("FILE")
                                .about("Flag if message is in a file.")
                                .short('f')
                                .long("file")
                                .takes_value(false)
                                .required(false),
                            Arg::new("OUTPUT")
                                .about(
                                    "Flag if output is to be directed \
                                     someplace other than standard output",
                                )
                                .short('o')
                                .long("output")
                                .takes_value(true)
                                .required(false),
                        ],
                    ),
                )
                .subcommand(
                    App::new("decertify")
                        .about("Decertify existing message")
                        .args(&[
                            Arg::new("MESSAGE")
                                .about("Message to certify")
                                .required(true)
                                .index(1),
                            Arg::new("SENDER")
                                .about("Identity to send from")
                                .required(true)
                                .index(2),
                            Arg::new("RECEIVER")
                                .about("Identity to send to")
                                .required(true)
                                .index(3),
                            Arg::new("PPC")
                                .about(
                                    "Optional path to specify which .ppc to \
                                     use",
                                )
                                .short('p')
                                .long("ppc")
                                .required(false),
                            Arg::new("FILE")
                                .about("Flag if message is in a file.")
                                .short('f')
                                .long("file")
                                .takes_value(false)
                                .required(false),
                            Arg::new("OUTPUT")
                                .about(
                                    "Flag if output is to be directed \
                                     someplace other than standard output",
                                )
                                .short('o')
                                .long("output")
                                .takes_value(true)
                                .required(false),
                            Arg::new("BUILD")
                                .about(
                                    "Flag to build all messages, both \
                                     certificates and proofs, possible using \
                                     this message.",
                                )
                                .short('b')
                                .long("build")
                                .takes_value(false)
                                .required(false),
                        ]),
                )
                .subcommand(
                    App::new("prove").about("Create a proof").args(&[
                        Arg::new("MESSAGE")
                            .about("Message to convert to proof")
                            .required(true)
                            .index(1),
                        Arg::new("SENDER")
                            .about("Identity to send from")
                            .required(true)
                            .index(2),
                        Arg::new("RECEIVER")
                            .about("Identity to send to")
                            .required(true)
                            .index(3),
                        Arg::new("PPC")
                            .about("Optional path to specify which .ppc to use")
                            .short('p')
                            .long("ppc")
                            .required(false),
                        Arg::new("FILE")
                            .about("Flag if message is in a file.")
                            .short('f')
                            .long("file")
                            .takes_value(false)
                            .required(false),
                        Arg::new("OUTPUT")
                            .about(
                                "Flag if output is to be directed someplace \
                                 other than standard output",
                            )
                            .short('o')
                            .long("output")
                            .takes_value(true)
                            .required(false),
                    ]),
                )
                .subcommand(
                    App::new("verify").about("Verify a proof").args(&[
                        Arg::new("MESSAGE")
                            .about("Verify that message contains valid proof")
                            .required(true)
                            .index(1),
                        Arg::new("PPC")
                            .about("Optional path to specify which .ppc to use")
                            .short('p')
                            .long("ppc")
                            .required(false),
                        Arg::new("FILE")
                            .about("Flag if message is in a file.")
                            .short('f')
                            .long("file")
                            .takes_value(false)
                            .required(false),
                    ]),
                )
                .subcommand(
                    App::new("validate").about("Validate a message").args(&[
                        Arg::new("MESSAGE")
                            .about("Validate that a message and its trace is valid.")
                            .required(true)
                            .index(1),
                        Arg::new("PPC")
                            .about("Optional path to specify which .ppc to use")
                            .short('p')
                            .long("ppc")
                            .required(false),
                        Arg::new("FILE")
                            .about("Flag if message is in a file.")
                            .short('f')
                            .long("file")
                            .takes_value(false)
                            .required(false),
                    ]),
                )
                .subcommand(
                    App::new("receive")
                        .about("Add a message to posessions")
                        .args(&[
                            Arg::new("MESSAGE")
                                .about("Add message to possessions.")
                                .required(true)
                                .index(1),
                            Arg::new("PPC")
                                .about(
                                    "Optional path to specify which .ppc to \
                                     use",
                                )
                                .short('p')
                                .long("ppc")
                                .required(false),
                            Arg::new("FILE")
                                .about("Flag if message is in a file.")
                                .short('f')
                                .long("file")
                                .takes_value(false)
                                .required(false),
                        ]),
                )
                .subcommand(
                    App::new("retrieve")
                        .about("Retrieve a message from posessions")
                        .args(&[
                            Arg::new("MESSAGE ID")
                                .about("ID of message receiver")
                                .required(true)
                                .index(1),
                            Arg::new("ITEM ID")
                                .about("ID of item receiver")
                                .required(true)
                                .index(2),
                            Arg::new("MESSAGE TYPE")
                                .about("Type of message to retreive, either Certificate, \
                                    Proof, or Atomic.")
                                .required(true)
                                .index(3),
                            Arg::new("ITEM TYPE")
                                .about("Type of item to retreive")
                                .required(true)
                                .index(4),
                            Arg::new("PPC")
                                .about(
                                    "Optional path to specify which .ppc to \
                                     use",
                                )
                                .short('p')
                                .long("ppc")
                                .required(false),
                            Arg::new("TEXT")
                                .about("Flag for readable body.")
                                .short('t')
                                .long("text")
                                .takes_value(false)
                                .required(false),
                            Arg::new("OUTPUT")
                                .about(
                                    "   Flag if output is to be directed \
                                     someplace other than standard output",
                                )
                                .short('o')
                                .long("output")
                                .takes_value(true)
                                .required(false),
                        ]),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("dsl", matches)) => cmd_dsl(matches),
        Some(("keygen", matches)) => cmd_keygen(matches),
        Some(("validator", matches)) => cmd_arch(matches),
        Some(("message", matches)) => cmd_msg(matches),
        _ => println!(
            "Unrecognized command, try 'ppc --help' for a list of valid \
             commands"
        ),
    }
}
